# debian-cdimages

As Debian wheezy version has reached the end of life, the usual repositories do not exist anymore and have been moved to the archive.

Open /etc/apt/sources.list

Comment existing sources

Add below sources

````
deb http://archive.debian.org/debian/ wheezy main non-free contrib
deb-src http://archive.debian.org/debian/ wheezy main non-free contrib
````

